import json, random, sys
from datetime import datetime

class Database:
	def __init__(self, log=sys.stdout):
		f = open('db.json')
		data = json.load(f)
		for key in data:
			setattr(self, key, data[key])
		self.log = log
	def random_quote(self):
		return random.choice(self.quotes)
	def addquote(self, quote):
		self.quotes.append(quote)
		self.save()
	def add_to_committee(self, member):
		if member not in self.committee:
			self.committee.append(member)
			self.save()
			return True
		return False
	def record_message(self, sender, message):
		datestr = datetime.now().strftime('%b %d, %Y %I:%M %p')
		with open('messages.log', 'a') as log:
			print(datestr, sender, message, file=log)
	def start_recording(self):
		with open('messages.log', 'a') as log:
			datestr = datetime.now().strftime('%b %d, %Y %I:%M %p')
			print('Recording started at',datestr, file=log)
	
	def stop_recording(self):
		with open('messages.log', 'a') as log:
			datestr = datetime.now().strftime('%b %d, %Y %I:%M %p')
			print('Recording stopped at',datestr, file=log)	
	
	def save(self):
		f = open('db.json', 'w')
		data = {}
		data['committee'] = self.committee
		data['quotes'] = self.quotes
		json.dump(data, f)
		f.close()
	
