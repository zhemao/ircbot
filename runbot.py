#!/usr/bin/env python3

from adibot import ADIBot
import sys

if __name__=='__main__':
	if len(sys.argv)>1:
		bot = ADIBot(open(sys.argv[1], 'a'))
	else: bot = ADIBot()
	bot.runbot()
