#!/bin/sh

WORKDIR=/home/zhehao/programs/ircbot
EXEC=./runbot.py
LOG=bot.log

cd $WORKDIR

start_bot () {
	if [ -f "bot.pid" ]; then
		echo "Bot already running"
	else
		echo "Starting Bot..."
		nohup $EXEC $LOG &
		echo $! > bot.pid
	fi
}

stop_bot () {
	if [ -f "bot.pid" ]; then
		"Stopping Bot..."
		kill `cat bot.pid`
		rm bot.pid
	else
		echo "Bot not running"
	fi
}


case $1 in 
	start)
		start_bot
		;;
	stop)
		stop_bot
		;;
	restart)
		stop_bot
		start_bot
		;;
	*)
		echo "Usage: $0 {start|stop|restart}"
esac
