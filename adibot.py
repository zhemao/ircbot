from ircbot import IrcBot
from helpers import *
from datetime import datetime
from database import Database
import sys

WELCOME_MSG = '''Welcome to the IRC channel of the Application Development Intiative. 
				For more information, say "!adi" or check out our website at http://
				adicu.com. For more commands say "!help".'''

ABOUT_ADI = '''The Application Development Intiative, or ADI, is a student group at 
			Columbia University for students interested in software development and 
			technological entrepreneurship. For more information, please check out our 
			website at http://adicu.com.'''

ABOUT_ME = '''I am an IRC bot, a computer program that sits on an IRC channel 
			and gives helpful messages to other users and logs conversations. 
			I was written in python 3 by Zhehao Mao. My code is distributed 
			under an MIT License and can be found at https://github.com/zhemao/ircbot/.
			'''

class ADIBot(IrcBot):
	def __init__(self, log=sys.stdout):
		super().__init__('adibot', 'adibot', "ADI IRC bot", 'irc.freenode.net')
		self.adminkey = 'hackcu2010'
		self.verbose = False
		self.log = log
		self.db = Database(self.log)
		self.recording = False
	def motdend(self):
		super().motdend()
		self.join('#adicu')
	def messaged(self, sender, recp, mess):
		super().messaged(sender, recp, mess)
		if mess == '!time':
			dtstr = datetime.now().strftime("It is now %I:%M%p on %A, %B %d, %Y. In other words, it's hacking time!")
			self.logmess('Gave the time to '+sender+'.')
			self.privmsg(sender, dtstr)
		elif mess == '!adi':
			self.logmess('Talked about adi to '+sender+'.')
			self.privmsg(sender, ABOUT_ADI)
		elif mess == '!committee':
			self.logmess('Talked about committee members to '+sender+'.')
			self.privmsg(sender, 'These are our committee members, go bother them: '+
							' '.join(self.db.committee))
		elif mess == '!help':
			self.logmess('Gave help to '+sender+'.')
			self.help(sender)
		elif mess == '!hello':
			self.logmess('Said hello back to '+sender+'.')
			self.privmsg(sender, "Hello, yourself!")
		elif mess == '!ping':
			self.logmess('Responded to ping from '+sender+'.')
			self.privmsg(sender, 'PONG!')
		elif mess == '!quote':
			self.logmess('Gave a quote to '+sender+'.')
			q = self.db.random_quote()
			self.privmsg(sender, q)
		elif mess == '!whoareyou':
			self.logmess('Told '+sender+' about me.')
			self.privmsg(sender, ABOUT_ME)
		elif recp == self.nick and mess.startswith('!admin'):
			self.admin(sender, mess)
			
		if self.recording and recp==self.room:
			self.db.record_message(sender, mess)
		
	def admin(self, sender, mess):
		parts = mess.split(' ')
		if len(parts) > 2 and parts[1] == self.adminkey:
			if parts[2] == 'stop':
				self.quit()
			elif parts[2] == 'committee':
				if self.db.add_to_committee(sender):
					self.privmsg(sender, 'You have been added as a committee member')
					self.logmess('Added '+sender+' to committee.')
				else: self.privmsg(sender, 'You are already in the committee.')
			elif parts[2] == 'addquote':
				self.db.addquote(' '.join(parts[3:]))
				self.privmsg(sender, 'Your quote has been added.')
				self.logmess('Added quote from '+sender+'.')
			elif parts[2] == 'recordon':
				self.recording = True
				self.privmsg(sender, 'Recording has been turned on')
				self.logmess('recording on')
				self.db.start_recording()
			elif parts[2] == 'recordoff':
				self.recording = False
				self.privmsg(sender, 'Recording has been turned off')
				self.db.stop_recording()
				self.logmess('recording off')
		else: 
			self.privmsg(sender, 'Incorrect admin password')
			self.logmess('Incorrect password attempt from '+sender)
		
	def help(self, user):
		self.privmsg(user, 'Some useful commands are:')
		commands = [('!time', 'Get the current date and time'), 
					('!adi', 'Learn more about the ADI'),
					('!committee', 'List the members of our executive committee who are currently connected.'),
					('!help', 'Display this help text'),
					('!quote', 'Read a funny quote from one of our events')]
		for com, desc in commands:
			self.privmsg(user, com+': '+desc)
		
	def post_join(self):
		super().post_join()
		self.privmsg(self.room, 'The ADI Bot has arrived!')
	def user_joined(self, user, room):
		super().user_joined(user, room)
		self.privmsg(user, "Hello "+user+'. '+WELCOME_MSG)


