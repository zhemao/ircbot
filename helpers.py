import random, string

def randomword(length, seed=string.ascii_lowercase+string.digits):
	return ''.join(random.sample(seed, length))
	
