import socket, io, re, sys, os, atexit, time
from datetime import date, datetime
from helpers import *
import random

DEFAULT_BUFSIZE = 1024
MSGRE = re.compile(r'^:(\w+)!~\w+@[.\-\w]+ PRIVMSG ([#@\w]+) :(.+)$')
QUITRE = re.compile(r'^:(\w+)!~\w+@[.\-\w]+ QUIT :(.+)$')
PINGRE = re.compile(r'^PING :([.\-\w]+)')
JOINRE = re.compile(r'^:(\w+)!~\w+@[.\-\w]+ JOIN :(#\w+)')

class Worker:
	pass

class IrcBot:
	log = sys.stdout
	verbose = False
	users = []
	def __init__(self, nick, ident, realname, host, port=6667):
		self.host = host
		self.port = port
		self.numsocks = 5
		self.nick = nick
		self.ident = ident
		self.realname = realname
		self.state = 'unconnected'
		self.logctr = 0
		self.routes = [
			(MSGRE, self.messaged),
			(PINGRE, self.pinged),
			(JOINRE, self.user_joined),
			(QUITRE, self.user_quit)
		]
		self.stopped = False
		
	def connect(self):
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.connect((self.host, self.port))
		self.sendcommand('NICK',[self.nick])
		self.sendcommand('USER',[self.ident, self.host, 'bla', self.realname])
		self.state = 'motd'
		
	def sendcommand(self, comm, args):
		args[-1] = ':'+args[-1]
		mess = comm+' '+(' '.join(args))+'\r\n'
		self.sock.send(mess.encode('utf-8'), socket.MSG_DONTWAIT)
		
	def read(self, bufsize=DEFAULT_BUFSIZE):
		raw = self.sock.recv(bufsize)
		return raw.decode('utf-8')
		
	def readchunks(self, bufsize=DEFAULT_BUFSIZE):
		while True:
			yield self.read(bufsize)
				
	def readlines(self):
		buf = ''
		for chunk in self.readchunks():
			buf+=chunk
			lines = buf.split('\n')
			buf = lines.pop()
			for line in lines:
				yield line.rstrip()
	
	def pinged(self, server):
		self.sendcommand('PONG', [server])
				
	def lineread(self, line):
		if self.verbose:
			self.logmess(line)
		if self.state == 'listening':
			self.route(line)
		elif self.state == 'motd':
			if 'End of /MOTD command' in line:
				self.state = 'ready'
				self.motdend()
		elif self.state == 'joining':
			if 'End of /NAMES list' in line:
				self.state = 'listening'
				self.post_join()
			else:
				m = re.match(r'^:[\w.\-]+ \d{3} '+self.nick+
								' @ '+self.room+' :(.+)$', line)
				if m:
					user = m.groups()[0]
					self.users.append(user)
		
	def motdend(self):
		self.logmess('MOTD ended')
		
	def user_joined(self, user, room):
		self.logmess(user+" joined "+room)
		self.users.append(user)
		
	def user_quit(self, user, mess):
		self.logmess(user+" quit with message "+mess)
		try:
			self.users.remove(user)
		except ValueError: pass
		
	def messaged(self, sender, recp, mess):
		pass
		
	def runbot(self):
		try:
			self.logmess('IRC Bot started '+date.today().isoformat())
			self.connect()
			for line in self.readlines():
				if self.stopped:
					break
				self.lineread(line)
		except KeyboardInterrupt:
			self.quit()
			self.logmess("exiting")
			sys.exit(0)
		except socket.error:
			sys.exit(0)
				
	def post_join(self):
		self.logmess("Joined "+self.room)

	def join(self, room):
		self.sendcommand('JOIN', [room])
		self.room = room
		self.state = 'joining'
		
	def privmsg(self, recip, mess):
		mess = re.sub('\s+', ' ', mess)
		self.sendcommand('PRIVMSG', [recip, mess])
		
	def route(self, line):
		for pat, func in self.routes:
			m = re.match(pat, line)
			if m:
				func(*m.groups())
				break
		
	def logmess(self, mess):
		datestr = datetime.now().strftime('[%b %d, %Y %I:%M %p]')
		print(datestr, mess, file=self.log)
		self.log.flush()
			
	def quit(self, mess='The Bot has quit'):
		self.logmess('Quitting '+date.today().isoformat())
		self.sendcommand('QUIT', [mess])
		self.sock.close()
			
